const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');

// Load Input Validation
const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');

// Load Professor model
const Professor = require('../../models/Professor');

// @route   GET api/professors/test
// @desc    Tests professors route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Professors Works' }));

// @route   POST api/professors/register
// @desc    Register professor
// @access  Public
router.post('/register', (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  Professor.findOne({ email: req.body.email }).then(professor => {
    if (professor) {
      errors.email = 'Email already exists';
      return res.status(400).json(errors);
    } else {
      const avatar = gravatar.url(req.body.email, {
        s: '200', // Size
        r: 'pg', // Rating
        d: 'mm' // Default
      });

      const newProfessor = new Professor({
        name: req.body.name,
        email: req.body.email,
        avatar,
        password: req.body.password
      });

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newProfessor.password, salt, (err, hash) => {
          if (err) throw err;
          newProfessor.password = hash;
          newProfessor
            .save()
            .then(professor => res.json(professor))
            .catch(err => console.log(err));
        });
      });
    }
  });
});

// @route   GET api/professors/login
// @desc    Login Professor / Returning JWT Token
// @access  Public
router.post('/login', (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  // Find professor by email
  Professor.findOne({ email }).then(professor => {
    // Check for professor
    if (!professor) {
      errors.email = 'Professor not found';
      return res.status(404).json(errors);
    }

    // Check Password
    bcrypt.compare(password, professor.password).then(isMatch => {
      if (isMatch) {
        // Professor Matched
        const payload = { id: professor.id, name: professor.name, avatar: professor.avatar }; // Create JWT Payload

        // Sign Token
        jwt.sign(
          payload,
          keys.secretOrKey,
          { expiresIn: 3600 },
          (err, token) => {
            res.json({
              success: true,
              token: 'Bearer ' + token
            });
          }
        );
      } else {
        errors.password = 'Password incorrect';
        return res.status(400).json(errors);
      }
    });
  });
});

// @route   GET api/professors/current
// @desc    Return current professor
// @access  Private
router.get(
  '/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    res.json({
      id: req.professor.id,
      name: req.professor.name,
      email: req.professor.email
    });
  }
);

module.exports = router;
